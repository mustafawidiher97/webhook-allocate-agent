<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utils\QiscusCrossApi;

class WebHookController extends Controller
{

    public function allocateAgent()
    {
        $unserved_rooms = collect(QiscusCrossApi::getUnservedRooms())->sortBy('created_at');
        if ($unserved_rooms->count() > 0) {
            $added_agent = $this->servedUnservedRooms($unserved_rooms);
            if (!empty($added_agent)) {
                return response()->json([
                    'data' => $added_agent
                ], 200);
            }
        }

        return response()->json([], 200);
    }

    public function setResolvedWebhookUrl(Request $request)
    {
        $resolved_webhook_url = $this->isHasResolvedWebhookUrl($request) ? $request->resolved_webhook_url : env('QISCUS_RESOLVED_WEBHOOK_URL');

        $config = QiscusCrossApi::getAppConfig();
        if (array_key_exists('mark_as_resolved_webhook_url', $config)) {
            if ($config['mark_as_resolved_webhook_url'] != $resolved_webhook_url) {
                QiscusCrossApi::postSetMarkAsResolvedWebhook($resolved_webhook_url, true);
            }
        } else {
            QiscusCrossApi::postSetMarkAsResolvedWebhook($resolved_webhook_url, true);
        }

        return response()->json([
            'info' => 'Berhasil set resolved webhook url',
            'resolved_webhook_url' => $resolved_webhook_url
        ], 200);
    }

    private function isHasResolvedWebhookUrl($request)
    {
        return $request->has('resolved_webhook_url') && !empty($request->resolved_webhook_url);
    }

    private function servedUnservedRooms($unserved_rooms)
    {
        foreach ($unserved_rooms as $unserved_room) {
            $room_id = $unserved_room['room_id'];

            $available_agents = collect(QiscusCrossApi::getAllAgents())->where('is_available', true)->where('current_customer_count', 0)->sortBy('total_customers');
            if ($available_agents->count() > 0) {
                $added_agent = QiscusCrossApi::postAssignAgent($room_id, $available_agents->first()['id']);
                return $added_agent;
            }
        }

        return [];
    }
}
