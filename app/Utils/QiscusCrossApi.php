<?php

namespace App\Utils;

use GuzzleHttp\Client;
use App\Exceptions\ResponseException;

class QiscusCrossApi
{

    //API GET
    public static function get($version, $api, $params = null)
    {
        $client = new Client([
            'verify' => false
        ]);
        try {
            $get = $client->get(env('QISCUS_MULTICHANNEL_URL') . "$version/$api$params", [
                'headers' => [
                    'Content-Type'      => 'application/json',
                    'Qiscus-App-Id'     => env('QISCUS_APP_ID'),
                    'Qiscus-Secret-Key' => env('QISCUS_SECRET_KEY')
                ],
                'http_errors' => false
            ]);
            $body = json_decode($get->getBody(), true);
            $statusCode = $get->getStatusCode();
        } catch (\Exception $e) {
            $body = [];
            $statusCode = 500;
        }
        return [$statusCode, $body];
    }

    public static function post($version, $api, $formParams)
    {
        $client = new Client([
            'verify' => false
        ]);
        try {
            $post = $client->post(env('QISCUS_MULTICHANNEL_URL') . "$version/" . $api, [
                'headers' => [
                    'Content-Type'      => 'application/x-www-form-urlencoded',
                    'Qiscus-App-Id'     => env('QISCUS_APP_ID'),
                    'Qiscus-Secret-Key' => env('QISCUS_SECRET_KEY')
                ],
                'form_params' => $formParams,
                'http_errors' => false
            ]);

            $body = json_decode($post->getBody(), true);
            $statusCode = $post->getStatusCode();
        } catch (\Exception $e) {
            $body = [];
            $statusCode = 500;
        }
        return [$statusCode, $body];
    }

    public static function getChannels()
    {
        list($statusCode, $body) = self::get('v2', 'channels');
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data'];
    }

    public static function getAppConfig()
    {
        // {{BaseUrl}}/api/v1/app/config
        list($statusCode, $body) = self::get('v1', 'app/config');
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data']['config'];
    }

    public static function getAvailableAgents($room_id, $limit = null, $cursor_after = null, $cursor_before = null, $is_available_in_room = null)
    {
        list($statusCode, $body) = self::get('v2', 'admin/service/available_agents',
            "?room_id=$room_id&limit=$limit&cursor_after=$cursor_after&cursor_before=$cursor_before&is_availanle_in_room=$is_available_in_room"
        );
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data']['agents'];
    }

    public static function getAllAgents($page = null, $limit = null, $search = null, $scope = null)
    {
        list($statusCode, $body) = self::get('v1', 'admin/agents', "?page=$page&limit=$limit&search=$search&scope=$scope");
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data']['agents']['data'];
    }

    public static function getUnservedRooms($page = null, $limit = null)
    {
        list($statusCode, $body) = self::get('v1', 'admin/service/get_unserved_rooms', "?page=$page&limit=$limit");
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data'];
    }

    public static function getAgent($userId)
    {
        list($statusCode, $body) = self::get('/v2', "admin/agent/$userId");
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data']['agent'];
    }

    public static function postAssignAgent($room_id, $agent_id, bool $replace_latest_agent = false, $max_agent = null)
    {
        $formParams = [
            'room_id'              => $room_id,
            'agent_id'             => $agent_id,
            'replace_latest_agent' => $replace_latest_agent
        ];
        if (!empty($max_agent)) $formParams['max_agent'] = $max_agent;
        list($statusCode, $body) = self::post('v1', 'admin/service/assign_agent', $formParams);
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }
        return $body['data'];
    }

    public static function postAllocateAssignAgent($room_id, $ignore_availability)
    {
        $formParams = [
            'room_id'             => $room_id,
            'ignore_availability' => $ignore_availability
        ];
        list($statusCode, $body) = self::post('v1', 'admin/service/allocate_assign_agent', $formParams);
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }

        return $body['data'];
    }

    public static function postSetMarkAsResolvedWebhook(string $webhook_url, bool $is_webhook_enabled)
    {
        $formParams = [
            'webhook_url'        => $webhook_url,
            'is_webhook_enabled' => $is_webhook_enabled
        ];
        list($statusCode, $body) = self::post('v1', 'app/webhook/mark_as_resolved', $formParams);
        if ($statusCode != 200) {
            throw new ResponseException(response()->json($body, $statusCode));
        }

        return $body['data'];
    }
}
