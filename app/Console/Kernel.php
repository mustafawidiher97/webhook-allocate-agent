<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            // jalankan fungsi pengecekan apakah ada customer yang berstatus reserved, urutkan berdasarkan waktu kedatangan
            // jika ada maka cek apakah ada agen yang available sesuai dengan user
            // jika ada maka alokasikan agent dengan jumlah pelayanan yang lebih rendah
        })->everyMinute();
    }
}
