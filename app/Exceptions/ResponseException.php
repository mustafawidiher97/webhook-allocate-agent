<?php
namespace App\Exceptions;

use Exception;

class ResponseException extends Exception
{
    protected $response;

    public function __construct($response)
    {
        $this->response = $response;
    }

    public function render()
    {
        return $this->response;
    }

    public function getResponse()
    {
        return $this->response;
    }
}
